`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:56:57 07/18/2021 
// Design Name: 
// Module Name:    led 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module led(
    input clock,
    output led1,
    output led2,
    output led3,
    output led4
    );

	reg [31:0] ledR;
	reg led;
	
	always @(posedge clock)
	begin
		if (ledR > 32'd25000000)
		begin
			ledR <= 32'd0;
			led <= !led;
		end
		else
			ledR <= ledR + 32'd1;
	end
	
	assign led1 = led;
	assign led2 = led;
	assign led3 = led;
	assign led4 = led;
	

endmodule

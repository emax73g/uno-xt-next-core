////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: led_synthesis.v
// /___/   /\     Timestamp: Sun Jul 18 09:03:28 2021
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/synthesis -ofmt verilog -sim led.ngc led_synthesis.v 
// Device	: xc6slx25-2-ftg256
// Input file	: led.ngc
// Output file	: /home/ise/share/blinky/blinky/netgen/synthesis/led_synthesis.v
// # of Modules	: 1
// Design Name	: led
// Xilinx        : /opt/Xilinx/14.7/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module led (
  clock, led
);
  input clock;
  output led;
  wire clock_BUFGP_0;
  wire ledO_27;
  wire \GND_1_o_ledR[31]_LessThan_2_o ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<0>_54 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<0>_55 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi_56 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<1>_57 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<1>_58 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi1_59 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<2>_60 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<2>_61 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi2_62 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<3>_63 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<3>_64 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi3_65 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<4>_66 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<4>_67 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<5> ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<5>_69 ;
  wire \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ;
  wire \Mcount_ledR_cy<1>_rt_98 ;
  wire \Mcount_ledR_cy<2>_rt_99 ;
  wire \Mcount_ledR_cy<3>_rt_100 ;
  wire \Mcount_ledR_cy<4>_rt_101 ;
  wire \Mcount_ledR_cy<5>_rt_102 ;
  wire \Mcount_ledR_cy<6>_rt_103 ;
  wire \Mcount_ledR_cy<7>_rt_104 ;
  wire \Mcount_ledR_cy<8>_rt_105 ;
  wire \Mcount_ledR_cy<9>_rt_106 ;
  wire \Mcount_ledR_cy<10>_rt_107 ;
  wire \Mcount_ledR_cy<11>_rt_108 ;
  wire \Mcount_ledR_cy<12>_rt_109 ;
  wire \Mcount_ledR_cy<13>_rt_110 ;
  wire \Mcount_ledR_cy<14>_rt_111 ;
  wire \Mcount_ledR_cy<15>_rt_112 ;
  wire \Mcount_ledR_cy<16>_rt_113 ;
  wire \Mcount_ledR_cy<17>_rt_114 ;
  wire \Mcount_ledR_cy<18>_rt_115 ;
  wire \Mcount_ledR_cy<19>_rt_116 ;
  wire \Mcount_ledR_cy<20>_rt_117 ;
  wire \Mcount_ledR_cy<21>_rt_118 ;
  wire \Mcount_ledR_cy<22>_rt_119 ;
  wire \Mcount_ledR_cy<23>_rt_120 ;
  wire \Mcount_ledR_xor<24>_rt_121 ;
  wire ledR_0_rstpot_122;
  wire ledR_1_rstpot_123;
  wire ledR_2_rstpot_124;
  wire ledR_3_rstpot_125;
  wire ledR_4_rstpot_126;
  wire ledR_5_rstpot_127;
  wire ledR_6_rstpot_128;
  wire ledR_7_rstpot_129;
  wire ledR_8_rstpot_130;
  wire ledR_9_rstpot_131;
  wire ledR_10_rstpot_132;
  wire ledR_11_rstpot_133;
  wire ledR_12_rstpot_134;
  wire ledR_13_rstpot_135;
  wire ledR_14_rstpot_136;
  wire ledR_15_rstpot_137;
  wire ledO_rstpot_138;
  wire ledR_16_rstpot_139;
  wire ledR_17_rstpot_140;
  wire ledR_18_rstpot_141;
  wire ledR_19_rstpot_142;
  wire ledR_20_rstpot_143;
  wire ledR_21_rstpot_144;
  wire [25 : 0] ledR;
  wire [24 : 0] Result;
  wire [0 : 0] Mcount_ledR_lut;
  wire [23 : 0] Mcount_ledR_cy;
  VCC   XST_VCC (
    .P(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<5> )
  );
  GND   XST_GND (
    .G(ledR[25])
  );
  FDR   ledR_22 (
    .C(clock_BUFGP_0),
    .D(Result[22]),
    .R(\GND_1_o_ledR[31]_LessThan_2_o ),
    .Q(ledR[22])
  );
  FDR   ledR_23 (
    .C(clock_BUFGP_0),
    .D(Result[23]),
    .R(\GND_1_o_ledR[31]_LessThan_2_o ),
    .Q(ledR[23])
  );
  FDR   ledR_24 (
    .C(clock_BUFGP_0),
    .D(Result[24]),
    .R(\GND_1_o_ledR[31]_LessThan_2_o ),
    .Q(ledR[24])
  );
  LUT5 #(
    .INIT ( 32'h00000001 ))
  \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<0>  (
    .I0(ledR[0]),
    .I1(ledR[1]),
    .I2(ledR[2]),
    .I3(ledR[3]),
    .I4(ledR[4]),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<0>_54 )
  );
  MUXCY   \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<0>  (
    .CI(ledR[25]),
    .DI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<5> ),
    .S(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<0>_54 ),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<0>_55 )
  );
  LUT5 #(
    .INIT ( 32'h00010000 ))
  \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<1>  (
    .I0(ledR[5]),
    .I1(ledR[7]),
    .I2(ledR[8]),
    .I3(ledR[9]),
    .I4(ledR[6]),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<1>_57 )
  );
  MUXCY   \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<1>  (
    .CI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<0>_55 ),
    .DI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi_56 ),
    .S(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<1>_57 ),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<1>_58 )
  );
  LUT5 #(
    .INIT ( 32'h00800000 ))
  \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<2>  (
    .I0(ledR[13]),
    .I1(ledR[11]),
    .I2(ledR[12]),
    .I3(ledR[10]),
    .I4(ledR[14]),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<2>_60 )
  );
  MUXCY   \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<2>  (
    .CI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<1>_58 ),
    .DI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi1_59 ),
    .S(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<2>_60 ),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<2>_61 )
  );
  LUT5 #(
    .INIT ( 32'h00080000 ))
  \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<3>  (
    .I0(ledR[16]),
    .I1(ledR[18]),
    .I2(ledR[15]),
    .I3(ledR[17]),
    .I4(ledR[19]),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<3>_63 )
  );
  MUXCY   \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<3>  (
    .CI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<2>_61 ),
    .DI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi2_62 ),
    .S(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<3>_63 ),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<3>_64 )
  );
  LUT5 #(
    .INIT ( 32'h00800000 ))
  \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<4>  (
    .I0(ledR[20]),
    .I1(ledR[21]),
    .I2(ledR[22]),
    .I3(ledR[23]),
    .I4(ledR[24]),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<4>_66 )
  );
  MUXCY   \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<4>  (
    .CI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<3>_64 ),
    .DI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi3_65 ),
    .S(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<4>_66 ),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<4>_67 )
  );
  MUXCY   \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<5>  (
    .CI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<4>_67 ),
    .DI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<5> ),
    .S(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<5> ),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<5>_69 )
  );
  MUXCY   \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>  (
    .CI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<5>_69 ),
    .DI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<5> ),
    .S(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<5> ),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 )
  );
  MUXCY   \Mcount_ledR_cy<0>  (
    .CI(ledR[25]),
    .DI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<5> ),
    .S(Mcount_ledR_lut[0]),
    .O(Mcount_ledR_cy[0])
  );
  XORCY   \Mcount_ledR_xor<0>  (
    .CI(ledR[25]),
    .LI(Mcount_ledR_lut[0]),
    .O(Result[0])
  );
  MUXCY   \Mcount_ledR_cy<1>  (
    .CI(Mcount_ledR_cy[0]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<1>_rt_98 ),
    .O(Mcount_ledR_cy[1])
  );
  XORCY   \Mcount_ledR_xor<1>  (
    .CI(Mcount_ledR_cy[0]),
    .LI(\Mcount_ledR_cy<1>_rt_98 ),
    .O(Result[1])
  );
  MUXCY   \Mcount_ledR_cy<2>  (
    .CI(Mcount_ledR_cy[1]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<2>_rt_99 ),
    .O(Mcount_ledR_cy[2])
  );
  XORCY   \Mcount_ledR_xor<2>  (
    .CI(Mcount_ledR_cy[1]),
    .LI(\Mcount_ledR_cy<2>_rt_99 ),
    .O(Result[2])
  );
  MUXCY   \Mcount_ledR_cy<3>  (
    .CI(Mcount_ledR_cy[2]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<3>_rt_100 ),
    .O(Mcount_ledR_cy[3])
  );
  XORCY   \Mcount_ledR_xor<3>  (
    .CI(Mcount_ledR_cy[2]),
    .LI(\Mcount_ledR_cy<3>_rt_100 ),
    .O(Result[3])
  );
  MUXCY   \Mcount_ledR_cy<4>  (
    .CI(Mcount_ledR_cy[3]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<4>_rt_101 ),
    .O(Mcount_ledR_cy[4])
  );
  XORCY   \Mcount_ledR_xor<4>  (
    .CI(Mcount_ledR_cy[3]),
    .LI(\Mcount_ledR_cy<4>_rt_101 ),
    .O(Result[4])
  );
  MUXCY   \Mcount_ledR_cy<5>  (
    .CI(Mcount_ledR_cy[4]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<5>_rt_102 ),
    .O(Mcount_ledR_cy[5])
  );
  XORCY   \Mcount_ledR_xor<5>  (
    .CI(Mcount_ledR_cy[4]),
    .LI(\Mcount_ledR_cy<5>_rt_102 ),
    .O(Result[5])
  );
  MUXCY   \Mcount_ledR_cy<6>  (
    .CI(Mcount_ledR_cy[5]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<6>_rt_103 ),
    .O(Mcount_ledR_cy[6])
  );
  XORCY   \Mcount_ledR_xor<6>  (
    .CI(Mcount_ledR_cy[5]),
    .LI(\Mcount_ledR_cy<6>_rt_103 ),
    .O(Result[6])
  );
  MUXCY   \Mcount_ledR_cy<7>  (
    .CI(Mcount_ledR_cy[6]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<7>_rt_104 ),
    .O(Mcount_ledR_cy[7])
  );
  XORCY   \Mcount_ledR_xor<7>  (
    .CI(Mcount_ledR_cy[6]),
    .LI(\Mcount_ledR_cy<7>_rt_104 ),
    .O(Result[7])
  );
  MUXCY   \Mcount_ledR_cy<8>  (
    .CI(Mcount_ledR_cy[7]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<8>_rt_105 ),
    .O(Mcount_ledR_cy[8])
  );
  XORCY   \Mcount_ledR_xor<8>  (
    .CI(Mcount_ledR_cy[7]),
    .LI(\Mcount_ledR_cy<8>_rt_105 ),
    .O(Result[8])
  );
  MUXCY   \Mcount_ledR_cy<9>  (
    .CI(Mcount_ledR_cy[8]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<9>_rt_106 ),
    .O(Mcount_ledR_cy[9])
  );
  XORCY   \Mcount_ledR_xor<9>  (
    .CI(Mcount_ledR_cy[8]),
    .LI(\Mcount_ledR_cy<9>_rt_106 ),
    .O(Result[9])
  );
  MUXCY   \Mcount_ledR_cy<10>  (
    .CI(Mcount_ledR_cy[9]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<10>_rt_107 ),
    .O(Mcount_ledR_cy[10])
  );
  XORCY   \Mcount_ledR_xor<10>  (
    .CI(Mcount_ledR_cy[9]),
    .LI(\Mcount_ledR_cy<10>_rt_107 ),
    .O(Result[10])
  );
  MUXCY   \Mcount_ledR_cy<11>  (
    .CI(Mcount_ledR_cy[10]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<11>_rt_108 ),
    .O(Mcount_ledR_cy[11])
  );
  XORCY   \Mcount_ledR_xor<11>  (
    .CI(Mcount_ledR_cy[10]),
    .LI(\Mcount_ledR_cy<11>_rt_108 ),
    .O(Result[11])
  );
  MUXCY   \Mcount_ledR_cy<12>  (
    .CI(Mcount_ledR_cy[11]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<12>_rt_109 ),
    .O(Mcount_ledR_cy[12])
  );
  XORCY   \Mcount_ledR_xor<12>  (
    .CI(Mcount_ledR_cy[11]),
    .LI(\Mcount_ledR_cy<12>_rt_109 ),
    .O(Result[12])
  );
  MUXCY   \Mcount_ledR_cy<13>  (
    .CI(Mcount_ledR_cy[12]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<13>_rt_110 ),
    .O(Mcount_ledR_cy[13])
  );
  XORCY   \Mcount_ledR_xor<13>  (
    .CI(Mcount_ledR_cy[12]),
    .LI(\Mcount_ledR_cy<13>_rt_110 ),
    .O(Result[13])
  );
  MUXCY   \Mcount_ledR_cy<14>  (
    .CI(Mcount_ledR_cy[13]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<14>_rt_111 ),
    .O(Mcount_ledR_cy[14])
  );
  XORCY   \Mcount_ledR_xor<14>  (
    .CI(Mcount_ledR_cy[13]),
    .LI(\Mcount_ledR_cy<14>_rt_111 ),
    .O(Result[14])
  );
  MUXCY   \Mcount_ledR_cy<15>  (
    .CI(Mcount_ledR_cy[14]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<15>_rt_112 ),
    .O(Mcount_ledR_cy[15])
  );
  XORCY   \Mcount_ledR_xor<15>  (
    .CI(Mcount_ledR_cy[14]),
    .LI(\Mcount_ledR_cy<15>_rt_112 ),
    .O(Result[15])
  );
  MUXCY   \Mcount_ledR_cy<16>  (
    .CI(Mcount_ledR_cy[15]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<16>_rt_113 ),
    .O(Mcount_ledR_cy[16])
  );
  XORCY   \Mcount_ledR_xor<16>  (
    .CI(Mcount_ledR_cy[15]),
    .LI(\Mcount_ledR_cy<16>_rt_113 ),
    .O(Result[16])
  );
  MUXCY   \Mcount_ledR_cy<17>  (
    .CI(Mcount_ledR_cy[16]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<17>_rt_114 ),
    .O(Mcount_ledR_cy[17])
  );
  XORCY   \Mcount_ledR_xor<17>  (
    .CI(Mcount_ledR_cy[16]),
    .LI(\Mcount_ledR_cy<17>_rt_114 ),
    .O(Result[17])
  );
  MUXCY   \Mcount_ledR_cy<18>  (
    .CI(Mcount_ledR_cy[17]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<18>_rt_115 ),
    .O(Mcount_ledR_cy[18])
  );
  XORCY   \Mcount_ledR_xor<18>  (
    .CI(Mcount_ledR_cy[17]),
    .LI(\Mcount_ledR_cy<18>_rt_115 ),
    .O(Result[18])
  );
  MUXCY   \Mcount_ledR_cy<19>  (
    .CI(Mcount_ledR_cy[18]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<19>_rt_116 ),
    .O(Mcount_ledR_cy[19])
  );
  XORCY   \Mcount_ledR_xor<19>  (
    .CI(Mcount_ledR_cy[18]),
    .LI(\Mcount_ledR_cy<19>_rt_116 ),
    .O(Result[19])
  );
  MUXCY   \Mcount_ledR_cy<20>  (
    .CI(Mcount_ledR_cy[19]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<20>_rt_117 ),
    .O(Mcount_ledR_cy[20])
  );
  XORCY   \Mcount_ledR_xor<20>  (
    .CI(Mcount_ledR_cy[19]),
    .LI(\Mcount_ledR_cy<20>_rt_117 ),
    .O(Result[20])
  );
  MUXCY   \Mcount_ledR_cy<21>  (
    .CI(Mcount_ledR_cy[20]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<21>_rt_118 ),
    .O(Mcount_ledR_cy[21])
  );
  XORCY   \Mcount_ledR_xor<21>  (
    .CI(Mcount_ledR_cy[20]),
    .LI(\Mcount_ledR_cy<21>_rt_118 ),
    .O(Result[21])
  );
  MUXCY   \Mcount_ledR_cy<22>  (
    .CI(Mcount_ledR_cy[21]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<22>_rt_119 ),
    .O(Mcount_ledR_cy[22])
  );
  XORCY   \Mcount_ledR_xor<22>  (
    .CI(Mcount_ledR_cy[21]),
    .LI(\Mcount_ledR_cy<22>_rt_119 ),
    .O(Result[22])
  );
  MUXCY   \Mcount_ledR_cy<23>  (
    .CI(Mcount_ledR_cy[22]),
    .DI(ledR[25]),
    .S(\Mcount_ledR_cy<23>_rt_120 ),
    .O(Mcount_ledR_cy[23])
  );
  XORCY   \Mcount_ledR_xor<23>  (
    .CI(Mcount_ledR_cy[22]),
    .LI(\Mcount_ledR_cy<23>_rt_120 ),
    .O(Result[23])
  );
  XORCY   \Mcount_ledR_xor<24>  (
    .CI(Mcount_ledR_cy[23]),
    .LI(\Mcount_ledR_xor<24>_rt_121 ),
    .O(Result[24])
  );
  OBUF   led_OBUF (
    .I(ledO_27),
    .O(led)
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<1>_rt  (
    .I0(ledR[1]),
    .O(\Mcount_ledR_cy<1>_rt_98 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<2>_rt  (
    .I0(ledR[2]),
    .O(\Mcount_ledR_cy<2>_rt_99 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<3>_rt  (
    .I0(ledR[3]),
    .O(\Mcount_ledR_cy<3>_rt_100 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<4>_rt  (
    .I0(ledR[4]),
    .O(\Mcount_ledR_cy<4>_rt_101 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<5>_rt  (
    .I0(ledR[5]),
    .O(\Mcount_ledR_cy<5>_rt_102 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<6>_rt  (
    .I0(ledR[6]),
    .O(\Mcount_ledR_cy<6>_rt_103 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<7>_rt  (
    .I0(ledR[7]),
    .O(\Mcount_ledR_cy<7>_rt_104 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<8>_rt  (
    .I0(ledR[8]),
    .O(\Mcount_ledR_cy<8>_rt_105 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<9>_rt  (
    .I0(ledR[9]),
    .O(\Mcount_ledR_cy<9>_rt_106 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<10>_rt  (
    .I0(ledR[10]),
    .O(\Mcount_ledR_cy<10>_rt_107 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<11>_rt  (
    .I0(ledR[11]),
    .O(\Mcount_ledR_cy<11>_rt_108 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<12>_rt  (
    .I0(ledR[12]),
    .O(\Mcount_ledR_cy<12>_rt_109 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<13>_rt  (
    .I0(ledR[13]),
    .O(\Mcount_ledR_cy<13>_rt_110 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<14>_rt  (
    .I0(ledR[14]),
    .O(\Mcount_ledR_cy<14>_rt_111 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<15>_rt  (
    .I0(ledR[15]),
    .O(\Mcount_ledR_cy<15>_rt_112 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<16>_rt  (
    .I0(ledR[16]),
    .O(\Mcount_ledR_cy<16>_rt_113 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<17>_rt  (
    .I0(ledR[17]),
    .O(\Mcount_ledR_cy<17>_rt_114 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<18>_rt  (
    .I0(ledR[18]),
    .O(\Mcount_ledR_cy<18>_rt_115 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<19>_rt  (
    .I0(ledR[19]),
    .O(\Mcount_ledR_cy<19>_rt_116 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<20>_rt  (
    .I0(ledR[20]),
    .O(\Mcount_ledR_cy<20>_rt_117 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<21>_rt  (
    .I0(ledR[21]),
    .O(\Mcount_ledR_cy<21>_rt_118 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<22>_rt  (
    .I0(ledR[22]),
    .O(\Mcount_ledR_cy<22>_rt_119 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_cy<23>_rt  (
    .I0(ledR[23]),
    .O(\Mcount_ledR_cy<23>_rt_120 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_ledR_xor<24>_rt  (
    .I0(ledR[24]),
    .O(\Mcount_ledR_xor<24>_rt_121 )
  );
  FD   ledR_0 (
    .C(clock_BUFGP_0),
    .D(ledR_0_rstpot_122),
    .Q(ledR[0])
  );
  FD   ledR_1 (
    .C(clock_BUFGP_0),
    .D(ledR_1_rstpot_123),
    .Q(ledR[1])
  );
  FD   ledR_2 (
    .C(clock_BUFGP_0),
    .D(ledR_2_rstpot_124),
    .Q(ledR[2])
  );
  FD   ledR_3 (
    .C(clock_BUFGP_0),
    .D(ledR_3_rstpot_125),
    .Q(ledR[3])
  );
  FD   ledR_4 (
    .C(clock_BUFGP_0),
    .D(ledR_4_rstpot_126),
    .Q(ledR[4])
  );
  FD   ledR_5 (
    .C(clock_BUFGP_0),
    .D(ledR_5_rstpot_127),
    .Q(ledR[5])
  );
  FD   ledR_6 (
    .C(clock_BUFGP_0),
    .D(ledR_6_rstpot_128),
    .Q(ledR[6])
  );
  FD   ledR_7 (
    .C(clock_BUFGP_0),
    .D(ledR_7_rstpot_129),
    .Q(ledR[7])
  );
  FD   ledR_8 (
    .C(clock_BUFGP_0),
    .D(ledR_8_rstpot_130),
    .Q(ledR[8])
  );
  FD   ledR_9 (
    .C(clock_BUFGP_0),
    .D(ledR_9_rstpot_131),
    .Q(ledR[9])
  );
  FD   ledR_10 (
    .C(clock_BUFGP_0),
    .D(ledR_10_rstpot_132),
    .Q(ledR[10])
  );
  FD   ledR_11 (
    .C(clock_BUFGP_0),
    .D(ledR_11_rstpot_133),
    .Q(ledR[11])
  );
  FD   ledR_12 (
    .C(clock_BUFGP_0),
    .D(ledR_12_rstpot_134),
    .Q(ledR[12])
  );
  FD   ledR_13 (
    .C(clock_BUFGP_0),
    .D(ledR_13_rstpot_135),
    .Q(ledR[13])
  );
  FD   ledR_14 (
    .C(clock_BUFGP_0),
    .D(ledR_14_rstpot_136),
    .Q(ledR[14])
  );
  FD   ledR_15 (
    .C(clock_BUFGP_0),
    .D(ledR_15_rstpot_137),
    .Q(ledR[15])
  );
  FD   ledO (
    .C(clock_BUFGP_0),
    .D(ledO_rstpot_138),
    .Q(ledO_27)
  );
  FD   ledR_16 (
    .C(clock_BUFGP_0),
    .D(ledR_16_rstpot_139),
    .Q(ledR[16])
  );
  FD   ledR_17 (
    .C(clock_BUFGP_0),
    .D(ledR_17_rstpot_140),
    .Q(ledR[17])
  );
  FD   ledR_18 (
    .C(clock_BUFGP_0),
    .D(ledR_18_rstpot_141),
    .Q(ledR[18])
  );
  FD   ledR_19 (
    .C(clock_BUFGP_0),
    .D(ledR_19_rstpot_142),
    .Q(ledR[19])
  );
  FD   ledR_20 (
    .C(clock_BUFGP_0),
    .D(ledR_20_rstpot_143),
    .Q(ledR[20])
  );
  FD   ledR_21 (
    .C(clock_BUFGP_0),
    .D(ledR_21_rstpot_144),
    .Q(ledR[21])
  );
  LUT5 #(
    .INIT ( 32'hAAAA8000 ))
  \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi3  (
    .I0(ledR[24]),
    .I1(ledR[22]),
    .I2(ledR[21]),
    .I3(ledR[20]),
    .I4(ledR[23]),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi3_65 )
  );
  LUT4 #(
    .INIT ( 16'h8880 ))
  \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi2  (
    .I0(ledR[19]),
    .I1(ledR[18]),
    .I2(ledR[17]),
    .I3(ledR[16]),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi2_62 )
  );
  LUT4 #(
    .INIT ( 16'h8000 ))
  \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi1  (
    .I0(ledR[14]),
    .I1(ledR[13]),
    .I2(ledR[12]),
    .I3(ledR[11]),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi1_59 )
  );
  LUT4 #(
    .INIT ( 16'hFFFE ))
  \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi  (
    .I0(ledR[6]),
    .I1(ledR[7]),
    .I2(ledR[8]),
    .I3(ledR[9]),
    .O(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lutdi_56 )
  );
  MUXCY   \Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_inv1_cy  (
    .CI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .DI(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<5> ),
    .S(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_lut<5> ),
    .O(\GND_1_o_ledR[31]_LessThan_2_o )
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_0_rstpot (
    .I0(Result[0]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_0_rstpot_122)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_1_rstpot (
    .I0(Result[1]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_1_rstpot_123)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_2_rstpot (
    .I0(Result[2]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_2_rstpot_124)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_3_rstpot (
    .I0(Result[3]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_3_rstpot_125)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_4_rstpot (
    .I0(Result[4]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_4_rstpot_126)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_5_rstpot (
    .I0(Result[5]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_5_rstpot_127)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_6_rstpot (
    .I0(Result[6]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_6_rstpot_128)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_7_rstpot (
    .I0(Result[7]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_7_rstpot_129)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_8_rstpot (
    .I0(Result[8]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_8_rstpot_130)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_9_rstpot (
    .I0(Result[9]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_9_rstpot_131)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_10_rstpot (
    .I0(Result[10]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_10_rstpot_132)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_11_rstpot (
    .I0(Result[11]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_11_rstpot_133)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_12_rstpot (
    .I0(Result[12]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_12_rstpot_134)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_13_rstpot (
    .I0(Result[13]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_13_rstpot_135)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_14_rstpot (
    .I0(Result[14]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_14_rstpot_136)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_15_rstpot (
    .I0(Result[15]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_15_rstpot_137)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_16_rstpot (
    .I0(Result[16]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_16_rstpot_139)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_17_rstpot (
    .I0(Result[17]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_17_rstpot_140)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_18_rstpot (
    .I0(Result[18]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_18_rstpot_141)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_19_rstpot (
    .I0(Result[19]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_19_rstpot_142)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_20_rstpot (
    .I0(Result[20]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_20_rstpot_143)
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  ledR_21_rstpot (
    .I0(Result[21]),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledR_21_rstpot_144)
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  ledO_rstpot (
    .I0(ledO_27),
    .I1(\Mcompar_GND_1_o_ledR[31]_LessThan_2_o_cy<6>_70 ),
    .O(ledO_rstpot_138)
  );
  BUFGP   clock_BUFGP (
    .I(clock),
    .O(clock_BUFGP_0)
  );
  INV   \Mcount_ledR_lut<0>_INV_0  (
    .I(ledR[0]),
    .O(Mcount_ledR_lut[0])
  );
endmodule


`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif


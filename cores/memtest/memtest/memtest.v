`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:18:16 08/23/2021 
// Design Name: 
// Module Name:    memtest 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

parameter addrBits = 25;
parameter dataBits = 16;

module memtest(
    input nReset,
    input clock,
    output [addrBits-1:0] ramAddr,
    inout [dataBits-1:0] ramData,
    output reg nRamOE,
    output reg nRamWE,
    output reg nRamCE,
	 output reg nRamLB,
	 output reg nRamUB,
	 output reg ledRed,
	 output reg ledYellow,
	 output reg ledGreen
    );
	 
parameter [15:0] S_START = 0, S_WRITE1 = 1, S_WRITE2 = 2, S_WRITE3 = 3, S_START_READ = 4, S_READ1 = 5, S_READ2 = 6, S_READ3 = 7, S_READ4 = 8, S_MEM_OK = 9, S_ERROR = 10;

reg [15:0] state, nextState; 	 

always @(posedge clock) 
begin
	if (!nReset)
		state <= S_START;
	else
		state <= nextState;

end

reg addrZero, addrInc;
reg [addrBits:0] address;
reg writing;

always @*
begin
	addrZero = 0;
	addrInc = 0;
   nRamOE = 1;
   nRamWE = 1;
   nRamCE = 1;
	nRamLB = 0;
	nRamUB = 0;
	ledRed = 0;
	ledYellow = 0;
	ledGreen = 0;
	writing = 0;
	nextState = state;
 	case (state)
		S_START:
		begin
			addrZero = 1;
			writing = 1;
			ledYellow = 1;
			nextState = S_WRITE1;
		end
		S_WRITE1:
		begin
			nRamWE = 0;
			nRamCE = 0;
			writing = 1;
			ledYellow = 1;
			nextState = S_WRITE2;
		end
		S_WRITE2:
		begin
			addrInc = 1;
			ledYellow = 1;
			writing = 1;
			nextState = S_WRITE3;
		end
		S_WRITE3:
		begin
			writing = 1;
			ledYellow = 1;
			if (address[addrBits] != 0)
				nextState = S_START_READ;
			else
				nextState = S_WRITE1;
		end
		S_START_READ:
		begin
			addrZero = 1;
			ledYellow = 1;
			nextState = S_READ1;
		end
		S_READ1:
		begin
			nRamCE = 0;
			nRamOE = 0;
			ledYellow = 1;
			nextState = S_READ2;
		end
		S_READ2:
		begin
			nRamCE = 0;
			nRamOE = 0;
			ledYellow = 1;
			if (ramData != address[dataBits-1:0])
				nextState = S_ERROR;
			else
				nextState = S_READ3;
		end
		S_READ3:
		begin
			addrInc = 1;
			ledYellow = 1;
			nextState = S_READ4;
		end
		S_READ4:
		begin
			ledYellow = 1;
			if (address[addrBits] != 0)
				nextState = S_MEM_OK;
			else
				nextState = S_READ1;
		end
		S_MEM_OK:
		begin
			ledGreen = 1;
		end
		S_ERROR:
		begin
			ledRed = 1;
		end
		default:
			nextState = state;
	endcase
end

always @(posedge addrInc, posedge addrZero)
begin
	if (addrZero)
		address <= 32'h0;
	else
		address <= address + 32'h1;

end

assign ramAddr = address[addrBits-1:0];

assign ramData = writing ? address[dataBits-1:0] : 32'hz; 

endmodule

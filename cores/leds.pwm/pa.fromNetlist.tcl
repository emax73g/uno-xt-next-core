
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name leds.pwm -dir "/home/ise/share/leds.pwm/planAhead_run_1" -part xc6slx16ftg256-2
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "/home/ise/share/leds.pwm/led_pwm_top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {/home/ise/share/leds.pwm} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "led_pwm_top.ucf" [current_fileset -constrset]
add_files [list {led_pwm_top.ucf}] -fileset [get_property constrset [current_run]]
link_design

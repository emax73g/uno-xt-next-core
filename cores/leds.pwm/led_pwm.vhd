----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:38:22 08/28/2021 
-- Design Name: 
-- Module Name:    ledPwm - rtl 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity clock_counter is
    port ( reset_n_i : 	in	std_logic;
			  clock_50_i : in std_logic;
           clock_o : out std_logic);
end clock_counter;

architecture rtl of clock_counter is
begin
	process (clock_50_i)
		constant period : integer := 5000;
		constant half_period : integer := period / 2;
		variable cnt:integer := 0;
		begin
			if (reset_n_i = '0')
			then
				cnt := 0;
			elsif (rising_edge(clock_50_i))
			then
				if (cnt = period - 1)
				then
					cnt := 0;
				else	
					cnt := cnt+1;
				end if;	
         end if;
			if (cnt < half_period)
			then
				clock_o <= '1';
			else
				clock_o <= '0';
			end if;	
		end process;
end rtl;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity led_pwm is
    port ( reset_n_i : 	in	std_logic;
			  clock_i : in std_logic;
           enable_i : in std_logic;
			  y1: in integer;
			  y2: in integer;
			  led_o : out std_logic);
end led_pwm;

architecture rtl of led_pwm is
begin
	process (y1, y2, enable_i)
	begin
	end process;
	process (clock_i)
		variable cnt: integer := 0;
		variable pwm: integer;
		constant period: integer := 10000;
		begin
			pwm := (y1 * y2) when (enable_i = '1') else 0;
			if (reset_n_i = '0')
			then
				cnt := 0;
			elsif (rising_edge(clock_i))
			then
				if (cnt = period - 1)
				then
					cnt := 0;
				else	
					cnt := cnt+1;
				end if;	
         end if;
			if (cnt < pwm)
			then
				led_o <= '1';
			else
				led_o <= '0';
			end if;	
		end process;
end rtl;

--Main
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity led_pwm_top is
	 generic (leds_kind : integer := 1);
    port ( reset_n_i : 	in	std_logic := '0';
			  clock_50_i : in  std_logic;
           led_red_o : out  std_logic;
           led_yellow_o : out  std_logic;
           led_green_o : out  std_logic;
           led_blue_o : out  std_logic);
end led_pwm_top;

architecture rtl of led_pwm_top is

signal pwm_clock: std_logic;

		-- 3mm round diffused LEDs
		--constant led_red_k : integer := 20;
		--constant led_yellow_k : integer := 50;
		--constant led_green_k : integer := 12;
		--constant led_blue_k : integer := 50;

		-- square color LEDs
		constant led_red_k : integer := 20;
		constant led_yellow_k : integer := 33;
		constant led_green_k : integer := 100;
		constant led_blue_k : integer := 20;

begin

	clock: entity work.clock_counter
	port map
	(
		reset_n_i  => reset_n_i,
		clock_50_i => clock_50_i,
		clock_o => pwm_clock
	);

	led_red_pwm: entity work.led_pwm
   port map
	(
		reset_n_i  => reset_n_i,
		--clock_i 	=> pwm_clock,
		clock_i 	=> clock_50_i,
		enable_i => '1',
		y1 		=> 100,
		y2			=> led_red_k,
		led_o 	=> led_red_o
	);

	led_yellow_pwm: entity work.led_pwm
   port map
	(
		reset_n_i  => reset_n_i,
		--clock_i 	=> pwm_clock,
		clock_i 	=> clock_50_i,
		enable_i => '1',
		y1 		=> 100,
		y2			=> led_yellow_k,
		led_o 	=> led_yellow_o
	);

	led_green_pwm: entity work.led_pwm
   port map
	(
		reset_n_i  => reset_n_i,
		--clock_i 	=> pwm_clock,
		clock_i 	=> clock_50_i,
		enable_i => '1',
		y1 		=> 100,
		y2			=> led_green_k,
		led_o 	=> led_green_o
	);

	led_blue_pwm: entity work.led_pwm
   port map
	(
		reset_n_i  => reset_n_i,
		--clock_i 	=> pwm_clock,
		clock_i 	=> clock_50_i,
		enable_i => '1',
		y1 		=> 100,
		y2			=> led_blue_k,
		led_o 	=> led_blue_o
	);

end rtl;




